#ifndef __OUTER_LOOP_H__
#define __OUTER_LOOP_H__

#include "power_sum_stats.h"
#include "jobs.h"

typedef ALIGN_DECLSPEC struct {
	MUTEX mutex;
	char *name;

	float *ul_skymap;
	float *circ_ul_skymap;
	float *snr_skymap;
	float *ul_freq_skymap;
	float *circ_ul_freq_skymap;
	float *snr_freq_skymap;
	float *snr_ul_skymap;
	float *max_weight_skymap;
	float *min_weight_skymap;
	float *weight_loss_fraction_skymap;
	float *ks_skymap;

	POWER_SUM_STATS *band_info;
	int *band_valid_count;
	int *band_masked_count;
	int *band_diverted_count;

	/* convenience info for keeping track of which ei is which */
	int first_chunk;
	int last_chunk;
	int veto_num;
	} EXTREME_INFO;


    
    
    
    
    

    
    
    

typedef struct S_OUTLIER_OUTPUT_INFO {
	char * kind;
	char * label;
	int index;
	char * set;
	int pi;
	int pps_count;
	int template_count;
	int first_bin;
	double min_gps;
	double max_gps;
	int skyband;
	double frequency;
	double spindown;
	double fdotdot;
	double freq_modulation_freq;
	double freq_modulation_depth;
	double freq_modulation_phase;
	double ra;
	double dec;
	double iota;
	double psi;
	double snr;
	double ul;
	double ll;
	double M;
	double S;
	double ks_value;
	int ks_count;
	double m1_neg;
	double m3_neg;
	double m4;
	int frequency_bin;
	double max_weight;
	double weight_loss_fraction;
	double max_ks_value;
	double max_m1_neg;
	double min_m1_neg;
	double max_m3_neg;
	double min_m3_neg;
	double max_m4;
	double min_m4;
	double max_weight_loss_fraction;

} OUTLIER_OUTPUT_INFO;



typedef struct S_BAND_INFO_OUTPUT {
    char * kind;
    char * label;
    int skyband;
    char * skyband_name;
    char * set;
    int first_bin;
    double frequency;
    double spindown;
    double fdotdot;
    double freq_modulation_freq;
    double freq_modulation_depth;
    double freq_modulation_phase;
    double ra;
    double dec;
    double iota;
    double psi;
    double snr;
    double ul;
    double ll;
    double M;
    double S;
    double ks_value;
    int ks_count;
    double m1_neg;
    double m3_neg;
    double m4;
    int frequency_bin;
    double max_weight;
    double weight_loss_fraction;
    double max_ks_value;
    double max_m1_neg;
    double min_m1_neg;
    double max_m3_neg;
    double min_m3_neg;
    double max_m4;
    double min_m4;
    double max_weight_loss_fraction;
    int valid_count;
    int masked_count;
    int diverted_count;
    int template_count;
    
} BAND_INFO_OUTPUT;


void outer_loop(void);



#endif
