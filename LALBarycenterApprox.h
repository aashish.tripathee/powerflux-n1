/*
 *  Copyright (C) 2012 Miroslav Shaltev, R Prix
 *  Copyright (C) 2007 Curt Cutler, Jolien Creighton, Reinhard Prix, Teviet Creighton
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#ifndef LALBarycenterApprox_h
#define LALBarycenterApprox_h

#include <stdio.h>
#include <math.h>
#include <lal/LALStdio.h>
#include <lal/LALStdlib.h>
#include <lal/LALConstants.h>
#include <lal/DetectorSite.h>
#include <lal/LALBarycenter.h>

#ifdef  __cplusplus
extern "C" {
#endif
       /* Static constants. */
    static const double earthYear   = 3600 * 24 * 365.256363004;
    static const double solarDay    = 3600 * 24;
    static const double siderealDay = 86164.09164;
    
#define fitRotation 0.01
#define maxRot cos(fitRotation)
    
    static const double eclPole[3] = { 0.00, -0.3977770, 0.9174821 };
    
    
       /* Function prototypes. */
    int InitApprox ( const char *dir );
    int XLALBarycenterEarthApprox ( EarthState *earth, const LIGOTimeGPS *tGPS, const EphemerisData *edat);
    int XLALBarycenterApprox ( EmissionTime *emit, const BarycenterInput *baryinput, const EarthState *earth);
    
    /*@}*/
    
#ifdef  __cplusplus
}
#endif      /* Close C++ protection */

#endif /* LALBarycenterApprox_h */
