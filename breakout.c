#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <regex.h>
#include <time.h>

#include <lal/LALInitBarycenter.h>
#include <lal/DetResponse.h>
#include <lal/Velocity.h>
#include <lal/DetectorSite.h>
#include <lal/LALBarycenter.h>
#include <lal/LALDetectors.h>
//#include "LALDetectorsPole.h"

#include <gsl/gsl_multifit.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_rng.h>

#include "global.h"
#include "polarization.h"
#include "LALBarycenterApprox.h"

char *earth_ephemeris;
char *sun_ephemeris;
EphemerisData *ephemeris;
LALDetector detector;
static const char ephemeris_path[] = ".";
static const int ref_time = 1130529362;

void * do_alloc(long a, long b)
{
    void *r;
    r=calloc(a,b);
    while(r==NULL){
        fprintf(stderr,"Could not allocate %ld units of %ld bytes each (%ld bytes total)\n", a, b, a*b);
        sleep(1);
        r=calloc(a,b);
    }
    return r;
}

LALDetector get_detector_struct(char *det)
{
    if(!strcasecmp("LHO", det)){
        return lalCachedDetectors[LALDetectorIndexLHODIFF];
    } else
        if(!strcasecmp("LLO", det)){
            return lalCachedDetectors[LALDetectorIndexLLODIFF];
        } else
            if(!strcasecmp("VIRGO", det)){
                return lalCachedDetectors[LALDetectorIndexVIRGODIFF];
            } else {
                fprintf(stderr,"Unrecognized detector site: \"%s\"\n", det);
                exit(-1);
            }
}

void init_ephemeris(void)
{
    earth_ephemeris=do_alloc(strlen(ephemeris_path)+20,1);
    sprintf(earth_ephemeris,"%s/earth00-19-DE405.dat",ephemeris_path);
    sun_ephemeris=do_alloc(strlen(ephemeris_path)+20,1);
    sprintf(sun_ephemeris,"%s/sun00-19-DE405.dat",ephemeris_path);
    
    ephemeris = XLALInitBarycenter(earth_ephemeris, sun_ephemeris);
}

void get_AM_response(INT64 gps, float latitude, float longitude, float orientation,
                     float *plus, float *cross)
{
    LALStatus status={level:0, statusPtr:NULL};
    LALSource source;
    LALDetAndSource det_and_source={NULL, NULL};
    LALDetAMResponse response;
    LIGOTimeGPS ligo_gps;
    
    memset(&ligo_gps, 0, sizeof(ligo_gps));
    ligo_gps.gpsSeconds=floor(gps);
    ligo_gps.gpsNanoSeconds=(gps-floor(gps))*1e9;
    
    memset(&source, 0, sizeof(source));
    source.equatorialCoords.system=COORDINATESYSTEM_EQUATORIAL;
    source.orientation=orientation;
    source.equatorialCoords.longitude=longitude;
    source.equatorialCoords.latitude=latitude;
    
    det_and_source.pDetector=&detector;
    det_and_source.pSource=&source;
    
    LALComputeDetAMResponse(&status, &response, &det_and_source, &ligo_gps);
    TESTSTATUS(&status);
    
    *cross=response.cross;
    *plus=response.plus;
    if(status.statusPtr)FREESTATUSPTR(&status);
}

void get_detector_vel(INT64 gps, float *velocity)
{
    LALStatus status={level:0, statusPtr:NULL};
    REAL8 det_velocity[3];
    int i;
    LIGOTimeGPS ligo_gps;
    
    memset(&ligo_gps, 0, sizeof(ligo_gps));
    
    ligo_gps.gpsSeconds=gps;
    ligo_gps.gpsNanoSeconds=0;
    LALDetectorVel(&status, det_velocity, &ligo_gps, detector, ephemeris);
    TESTSTATUS(&status);
    
    for(i=0;i<3;i++)velocity[i]=det_velocity[i];
    if(status.statusPtr)FREESTATUSPTR(&status);
}

void get_emission_time(EmissionTime *emission_time, EarthState *earth_state, double ra, double dec, double dInv, char *detector, LIGOTimeGPS tGPS)
{
    BarycenterInput baryinput;
    LALStatus status={level:0, statusPtr:NULL};
    
    memset(&baryinput, 0, sizeof(baryinput));
    baryinput.tgps=tGPS;
    baryinput.site=get_detector_struct(detector);
    baryinput.site.location[0]=baryinput.site.location[0]/LAL_C_SI;
    baryinput.site.location[1]=baryinput.site.location[1]/LAL_C_SI;
    baryinput.site.location[2]=baryinput.site.location[2]/LAL_C_SI;
    baryinput.alpha=ra;
    baryinput.delta=dec;
    baryinput.dInv=dInv;
    
    XLALBarycenter(emission_time, &baryinput, earth_state);
    TESTSTATUS(&status);
    if(status.statusPtr)FREESTATUSPTR(&status);
}

int main ( int argc, char *argv[] ) {
    FILE *params;
    char buff[1000];
    params = fopen(argv[1],"r");
    if (!params)
        return 1;
    init_ephemeris();
    detector = get_detector_struct(argv[2]);
    float plus;
    float cross;
    int idx;
    INT64 gps;
    double ra;
    double dec;
    double orient;
    double dInv;
    double te;
    EmissionTime emission_time;
    LIGOTimeGPS tGPS;
    EarthState earth_state;
    BarycenterInput baryinput;
    
    if (strcmp(argv[3],"AM_RESP")==0) {
        printf("gps\tra\tdec\torientation\tplus\tcross\n");
        while (fgets(buff, 1000, params) != NULL) {
            gps = (INT64)atoi(strtok(buff,"\t"));
            ra = atof(strtok(NULL,"\t"));
            dec = atof(strtok(NULL,"\t"));
            orient = atof(strtok(NULL,"\t"));
            get_AM_response(gps,dec,ra,orient,&plus,&cross);
            printf("%i\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\n",gps,ra,dec,orient,plus,cross);
        }
        
    } else if (strcmp(argv[3],"BARY_EARTH")==0) {
        printf("idx\tgps\tra\tdec\tdInv\t"
               "earth_px\tearth_py\tearth_pz\t"
               "earth_vx\tearth_vy\tearth_vz\t"
               "se_x\tse_y\tse_z\t"
               "dse_x\tdse_y\tdse_z\t"
               "gmstRad\tgastRad\t"
               "delpsi\tdeleps\t"
               "tzeA\tthetaA\tzA\t"
               "rse\tdrse\t"
               "einstein\tdeinstein\t"
               "emitT\n");
        while (fgets(buff, 1000, params) != NULL) {
            // Index can be in exp notation (eg 1e5), so read as float
            idx = (int)atof(strtok(buff,"\t"));
            gps = (INT64)atoi(strtok(NULL,"\t"));
            ra = atof(strtok(NULL,"\t"));
            dec = atof(strtok(NULL,"\t"));
            dInv = atof(strtok(NULL,"\t"));
            memset(&tGPS, 0, sizeof(tGPS));
            tGPS.gpsSeconds=floor(gps);
            tGPS.gpsNanoSeconds=(gps-floor(gps))*1e9;
            if (atoi(argv[4]) == 1) {
                XLALBarycenterEarthApprox(&earth_state, &tGPS, ephemeris);
            } else {
                XLALBarycenterEarth(&earth_state, &tGPS, ephemeris);
            }
            get_emission_time(&emission_time, &(earth_state), ra, dec, dInv, argv[2], tGPS);
            te=(emission_time.te.gpsSeconds-ref_time)+((double)(1e-9))*emission_time.te.gpsNanoSeconds;
            printf("%i\t%i\t%.16e\t%.16e\t%.16e\t"   // Input
                   "%.16e\t%.16e\t%.16e\t" // Position
                   "%.16e\t%.16e\t%.16e\t" // Velocity
                   "%.16e\t%.16e\t%.16e\t" // Earth-Sun
                   "%.16e\t%.16e\t%.16e\t" // Earth-Sun Rate
                   "%.16e\t%.16e\t"        // Rad
                   "%.16e\t%.16e\t"        // Del
                   "%.16e\t%.16e\t%.16e\t" // A
                   "%.16e\t%.16e\t"        // RSE
                   "%.16e\t%.16e\t"        // Einstein
                   "%.16e\n",              // Emission
                   idx,gps,ra,dec,dInv,
                   earth_state.posNow[0],
                   earth_state.posNow[1],
                   earth_state.posNow[2],
                   earth_state.velNow[0],
                   earth_state.velNow[1],
                   earth_state.velNow[2],
                   earth_state.se[0],
                   earth_state.se[1],
                   earth_state.se[2],
                   earth_state.dse[0],
                   earth_state.dse[1],
                   earth_state.dse[2],
                   earth_state.gmstRad,
                   earth_state.gastRad,
                   earth_state.delpsi,
                   earth_state.deleps,
                   earth_state.tzeA,
                   earth_state.thetaA,
                   earth_state.zA,
                   earth_state.rse,
                   earth_state.drse,
                   earth_state.einstein,
                   earth_state.deinstein,te);
        }
        
    } else if (strcmp(argv[3],"BARY_EMIT")==0) {
        memset(&baryinput, 0, sizeof(baryinput));
        baryinput.site=detector;
        baryinput.site.location[0]=baryinput.site.location[0]/LAL_C_SI;
        baryinput.site.location[1]=baryinput.site.location[1]/LAL_C_SI;
        baryinput.site.location[2]=baryinput.site.location[2]/LAL_C_SI;
        if (atoi(argv[4]) == 1) {
            InitApprox("tables/");
        }
        
        printf("idx\tgps\tra\tdec\tdInv\t"
               "deltaT\tte_s\tte_ns\ttdot\t"
               "detector_px\tdetector_py\tdetector_pz\t"
               "detector_vx\tdetector_vy\tdetector_vz\t"
               "se_x\tse_y\tse_z\t"
               "dse_x\tdse_y\tdse_z\t"
               "roemer\tdroemer\t"
               "shapiro\tdshapiro\t"
               "erot\tderot\n");
        while (fgets(buff, 1000, params) != NULL) {
            // Index can be in exp notation (eg 1e5), so read as float
            idx = (int)atof(strtok(buff,"\t"));
            gps = (INT64)atoi(strtok(NULL,"\t"));
            ra = atof(strtok(NULL,"\t"));
            dec = atof(strtok(NULL,"\t"));
            dInv = atof(strtok(NULL,"\t"));
            memset(&tGPS, 0, sizeof(tGPS));
            tGPS.gpsSeconds=floor(gps);
            tGPS.gpsNanoSeconds=0;
            XLALBarycenterEarth(&earth_state, &tGPS, ephemeris);
            
            baryinput.tgps=tGPS;
            baryinput.alpha=ra;
            baryinput.delta=dec;
            baryinput.dInv=dInv;
            
            if (atoi(argv[4]) == 1) {
                XLALBarycenterApprox(&emission_time, &baryinput, &earth_state);
            } else {
                XLALBarycenter(&emission_time, &baryinput, &earth_state);
            }
            te=(emission_time.te.gpsSeconds)+((double)(1e-9))*emission_time.te.gpsNanoSeconds;
            printf("%i\t%i\t%.16e\t%.16e\t%.16e\t"   // Input
                   "%.16e\t%i\t%i\t%.16e\t"    // Time
                   "%.16e\t%.16e\t%.16e\t"     // Position
                   "%.16e\t%.16e\t%.16e\t"     // Velocity
                   "%.16e\t%.16e\t%.16e\t"     // Earth-Sun
                   "%.16e\t%.16e\t%.16e\t"     // Earth-Sun Rate
                   "%.16e\t%.16e\t"            // Roemer
                   "%.16e\t%.16e\t"            // Shapiro
                   "%.16e\t%.16e\n",           // Erot
                   idx,gps,ra,dec,dInv,
                   emission_time.deltaT,
                   emission_time.te.gpsSeconds,
                   emission_time.te.gpsNanoSeconds,
                   emission_time.tDot,
                   emission_time.rDetector[0],
                   emission_time.rDetector[1],
                   emission_time.rDetector[2],
                   emission_time.vDetector[0],
                   emission_time.vDetector[1],
                   emission_time.vDetector[2],
                   earth_state.se[0],
                   earth_state.se[1],
                   earth_state.se[2],
                   earth_state.dse[0],
                   earth_state.dse[1],
                   earth_state.dse[2],
                   emission_time.roemer,
                   emission_time.droemer,
                   emission_time.shapiro,
                   emission_time.dshapiro,
                   emission_time.erot,
                   emission_time.derot);
        }
        
    } else if (strcmp(argv[3],"BARY_TEST")==0) {
        memset(&baryinput, 0, sizeof(baryinput));
        baryinput.site=detector;
        baryinput.site.location[0]=baryinput.site.location[0]/LAL_C_SI;
        baryinput.site.location[1]=baryinput.site.location[1]/LAL_C_SI;
        baryinput.site.location[2]=baryinput.site.location[2]/LAL_C_SI;
        tGPS.gpsNanoSeconds=0;
        clock_t start = clock(), diff;
        while (fgets(buff, 1000, params) != NULL) {
            // Index can be in exp notation (eg 1e5), so read as float
            idx = (int)atof(strtok(buff,"\t"));
            gps = (INT64)atoi(strtok(buff,"\t"));
            ra = atof(strtok(NULL,"\t"));
            dec = atof(strtok(NULL,"\t"));
            dInv = atof(strtok(NULL,"\t"));
            memset(&tGPS, 0, sizeof(tGPS));
            tGPS.gpsSeconds=floor(gps);
            tGPS.gpsNanoSeconds=(gps-floor(gps))*1e9;
//            XLALBarycenterEarthApprox(&earth_state, &tGPS, ephemeris);
            XLALBarycenterEarth(&earth_state, &tGPS, ephemeris);
            baryinput.tgps=tGPS;
            baryinput.alpha=ra;
            baryinput.delta=dec;
            baryinput.dInv=dInv;
            XLALBarycenterApprox(&emission_time, &baryinput, &earth_state);
        }
        diff = clock() - start;
        
        int msec = diff * 1000 / CLOCKS_PER_SEC;
        printf("Approx function time taken %d seconds %d milliseconds\n", msec/1000, msec%1000);
        
        rewind(params);
        start = clock();
        while (fgets(buff, 1000, params) != NULL) {
            // Index can be in exp notation (eg 1e5), so read as float
            idx = (int)atof(strtok(buff,"\t"));
            gps = (INT64)atoi(strtok(buff,"\t"));
            ra = atof(strtok(NULL,"\t"));
            dec = atof(strtok(NULL,"\t"));
            dInv = atof(strtok(NULL,"\t"));
            memset(&tGPS, 0, sizeof(tGPS));
            tGPS.gpsSeconds=floor(gps);
            tGPS.gpsNanoSeconds=(gps-floor(gps))*1e9;
            XLALBarycenterEarth(&earth_state, &tGPS, ephemeris);
            baryinput.tgps=tGPS;
            baryinput.alpha=ra;
            baryinput.delta=dec;
            baryinput.dInv=dInv;
            XLALBarycenter(&emission_time, &baryinput, &earth_state);
        }
        diff = clock() - start;
        
        msec = diff * 1000 / CLOCKS_PER_SEC;
        printf("True function time taken %d seconds %d milliseconds\n", msec/1000, msec%1000);
        
    }
    fclose(params);
}
