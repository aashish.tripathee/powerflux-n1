/*
 * Copyright (C) 2012 Miroslav Shaltev, R Prix
 *  Copyright (C) 2007 Curt Cutler, Jolien Creighton, Reinhard Prix, Teviet Creighton
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#include <lal/Date.h>
#include <lal/LALBarycenter.h>
#include <math.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <string.h>
#include <limits.h>
#include <float.h>

#include "LALBarycenterApprox.h"

#define OBLQ 0.40909280422232891e0; /* obliquity of ecliptic at JD 245145.0* in radians */;
#define NTERMS 34 /* Number of fit terms */
#define MAXFITS 255 /* Size of array storing fit times */
#define TCOVER 5e5 /* Number of seconds covered by each fit */

EmissionTime saved_emit;
BarycenterInput saved_bary;
REAL8 saved_s[3];
int saved_idx;
int fitCount = 0;
int i;
int fitTimes[MAXFITS];
double **xCoef;
double **yCoef;
double **zCoef;
FILE *fp;

int
AddElem (int *array, const int length, const int add )
{
    int i;
    if (length >= MAXFITS) {
        XLALPrintError ("Number of GPS times provided exceeds limit: %i\n", MAXFITS);
        XLAL_ERROR ( XLAL_EINVAL );
        return(-1);
    }
    for (i = 0; array[i] < add && i < length; i++);
    for (int j = length; j > i; j--) {
        array[j] = array[j-1];
    }
    array[i] = add;

    return(0);
}

int
InitApprox ( const char *dir )
{
    /* TODO: Add check for duplicate times – problem if init is called several times */
    int tAdd;
    DIR *dp;
    struct dirent *ep;

    regex_t tabPatt;
    regmatch_t groups[2];
    char *cursor;
    int len;

    regcomp(&tabPatt, "xTable-([[:digit:]]+).txt", REG_EXTENDED);

    fprintf(stderr,"Opening dir %s.\n", dir);
    dp = opendir(dir);
    if (dp != NULL) {
        while ((ep = readdir(dp))) {
            cursor = ep->d_name;
            if (regexec(&tabPatt, cursor, 2, groups, 0)) {
                continue;
            }

            len = groups[1].rm_eo - groups[1].rm_so;
            char *buffer;
            buffer = (char*) malloc(len + 1);
            memcpy(buffer, &ep->d_name[groups[1].rm_so], len);
            buffer[len] = '\0';
            sscanf(buffer, "%i", &tAdd);
            AddElem(fitTimes, fitCount, tAdd);
            free(buffer);

            fitCount++;
        }
        closedir(dp);
    } else {
        XLALPrintError ("%s: invalid coefficient directory\n", dir );
        XLAL_ERROR ( XLAL_EINVAL );
        return(-1);
    }

    xCoef = (double**) calloc(fitCount, sizeof(double*));
    yCoef = (double**) calloc(fitCount, sizeof(double*));
    zCoef = (double**) calloc(fitCount, sizeof(double*));

    return(0);
}

int
ReadCoefs (const char *filename, double **coefs, const int tIdx) {
    fprintf(stderr,"Opening file %s.\n", filename);
    coefs[tIdx] = (double*) malloc(NTERMS*sizeof(double));
    int i = 0;
    char buffer[255];
    char *delim;
    FILE *fp = fopen(filename, "r+");
    if (fp == NULL) {
        XLALPrintError ("Error reading file %s.\n", filename);
        XLAL_ERROR ( XLAL_EINVAL );
        return(-1);
    }
    while (i < NTERMS) {
        fgets(buffer, sizeof(buffer), fp);
        if (buffer == NULL) {
            XLALPrintError ("Error reading line %i of %s.\n", i, filename);
            XLAL_ERROR ( XLAL_EINVAL );
            fclose(fp);
            return(-1);
        }
        if (strncmp(buffer, "#", 1) == 0) {
            // Comment line
            continue;
        } 
        strtok(buffer, "\t"); // Throw out var name
        delim = strtok(NULL, "\n");
        if (delim == NULL) {
            XLALPrintError ("Error reading coefficient %i of %s.\n", i, filename);
            XLAL_ERROR ( XLAL_EINVAL );
            return(-1);
            fclose(fp);
        }
        sscanf(delim, "%lg", &coefs[tIdx][i]);
        i++;
    }
    fclose(fp);
    return(0);
}

// Look for the fit with the closest start time BEFORE requested time.
int
PickFit (const int timeReq) {
    int mini = -1;
    int minDiff = INT_MAX;

    for (int i = 0; i < fitCount; i++) {
        if (timeReq - fitTimes[i] < minDiff &&
            timeReq >= fitTimes[i]) {
            mini = i;
            minDiff = timeReq - fitTimes[i];
        }
    }
    if (minDiff <= TCOVER) {
        return(mini);
    } else {
        XLALPrintError ("No fit covering t = %i.\n", timeReq);
        XLAL_ERROR ( XLAL_EINVAL );
    }
}

// Read coefs for x/y/z at given time.
int
LoadCoefs (const int timeReq) {
    int mini = PickFit(timeReq);
    int err = 0;
    char filename[255];
    fprintf(stderr, "Matched time: %i\n", fitTimes[mini]);
    snprintf(filename, sizeof(filename), "tables/xTable-%i.txt", fitTimes[mini]);
    err += ReadCoefs(filename, xCoef, mini);
    snprintf(filename, sizeof(filename), "tables/yTable-%i.txt", fitTimes[mini]);
    err += ReadCoefs(filename, yCoef, mini);
    snprintf(filename, sizeof(filename), "tables/zTable-%i.txt", fitTimes[mini]);
    err += ReadCoefs(filename, zCoef, mini);
    return(err);
}

int
BaryEqual ( const BarycenterInput *bary1,
            const BarycenterInput *bary2)
{
    if (bary1->site.location[0]!=bary2->site.location[0] ||
        bary1->site.location[1]!=bary2->site.location[1] ||
        bary1->site.location[2]!=bary2->site.location[2]) {
        return -1;
    }
    if (fabs(bary1->dInv-bary2->dInv) > bary1->dInv*bary2->dInv) { // abs(d1 - d2) > 1 sec
        return -1;
    }
    return 0;
}

REAL8
RotationFit ( const double** coef,
              const int tIdx,
              const REAL8* s,
              const double dt,
              const double phi,
              const BarycenterInput *bary,
              const EarthState *earth,
              const EmissionTime *emit)
{
    
    REAL8 e1sSol = s[0] * sin(2*M_PI*dt/solarDay);
    REAL8 e1cSol = s[0] * cos(2*M_PI*dt/solarDay);
    REAL8 e2sSol = s[1] * sin(2*M_PI*dt/solarDay);
    REAL8 e2cSol = s[1] * cos(2*M_PI*dt/solarDay);
    REAL8 e3sSol = s[2] * sin(2*M_PI*dt/solarDay);
    REAL8 e3cSol = s[2] * cos(2*M_PI*dt/solarDay);
    
    REAL8 e1sSid = s[0] * sin(2*M_PI*dt/siderealDay);
    REAL8 e1cSid = s[0] * cos(2*M_PI*dt/siderealDay);
    REAL8 e2sSid = s[1] * sin(2*M_PI*dt/siderealDay);
    REAL8 e2cSid = s[1] * cos(2*M_PI*dt/siderealDay);
    REAL8 e3sSid = s[2] * sin(2*M_PI*dt/siderealDay);
    REAL8 e3cSid = s[2] * cos(2*M_PI*dt/siderealDay);
    
    REAL8 dirDotEclsYr = (s[0] * eclPole[0] + s[1] * eclPole[1] + s[2] * eclPole[2]) * sin(2*M_PI*dt/earthYear);
    REAL8 dirDotEclcYr = (s[0] * eclPole[0] + s[1] * eclPole[1] + s[2] * eclPole[2]) * cos(2*M_PI*dt/earthYear);
    
    REAL8 dirDotVel = (s[0] * emit->vDetector[0] +
                       s[1] * emit->vDetector[1] +
                       s[2] * emit->vDetector[2]);
    REAL8 dirDotVelsYr = dirDotVel * sin(2*M_PI*dt/earthYear);
    REAL8 dirDotVelcYr = dirDotVel * cos(2*M_PI*dt/earthYear);

    REAL8 e1 = s[0];
    REAL8 e2 = s[1];
    REAL8 e3 = s[2];
    REAL8 se_x = earth->se[0];
    REAL8 se_y = earth->se[1];
    REAL8 se_z = earth->se[2];
    
    return (coef[tIdx][ 0]*e1 + coef[tIdx][ 1]*e2 + coef[tIdx][ 2]*e3 + 
            coef[tIdx][ 3]*e1sSol + coef[tIdx][ 4]*e2sSol + coef[tIdx][ 5]*e3sSol + 
            coef[tIdx][ 6]*e1cSol + coef[tIdx][ 7]*e2cSol + coef[tIdx][ 8]*e3cSol + 
            coef[tIdx][ 9]*e1sSid + coef[tIdx][10]*e2sSid + coef[tIdx][11]*e3sSid + 
            coef[tIdx][12]*e1cSid + coef[tIdx][13]*e2cSid + coef[tIdx][14]*e3cSid + 
            coef[tIdx][15]*e1*se_x + coef[tIdx][16]*e2*se_x + coef[tIdx][17]*e3*se_x + 
            coef[tIdx][18]*e1*se_y + coef[tIdx][19]*e2*se_y + coef[tIdx][20]*e3*se_y + 
            coef[tIdx][21]*e1*se_z + coef[tIdx][22]*e2*se_z + coef[tIdx][23]*e3*se_z + 
            coef[tIdx][24]*e1*se_x*phi + coef[tIdx][25]*e2*se_y*phi + coef[tIdx][26]*e3*se_z*phi + 
            coef[tIdx][27]*dirDotEclsYr + coef[tIdx][28]*dirDotEclcYr + 
            coef[tIdx][29]*dirDotVelsYr + coef[tIdx][30]*dirDotVelcYr + 
            coef[tIdx][31]*dt*phi*dirDotVel + coef[tIdx][32]*phi*dirDotVel + coef[tIdx][33]*dt*dirDotVel)*phi;
}

int
XLALBarycenterEarthApprox ( EarthState *earth, 		/**< [out] the earth's state at time tGPS */
                     const LIGOTimeGPS *tGPS, 		/**< [in] GPS time tgps */
                     const EphemerisData *edat) 	/**< [in] ephemeris-files */
{
    
    XLALPrintError ("%s: Not yet supported.\n", __func__ );
    XLAL_ERROR ( XLAL_EINVAL );
    
    return XLAL_SUCCESS;
    
} /* XLALBarycenterEarthApprox() */

int
XLALBarycenterApprox ( EmissionTime *emit, 		         	/**< [out] emission-time information */
                       const BarycenterInput *baryinput, 	/**< [in] info about detector and source-location */
                       const EarthState *earth)      		/**< [in] earth-state (from XLALBarycenterEarth()) */
{
    
    REAL8 sinTheta;  /* sin(theta) = sin(pi/2 - delta) */
    REAL8 s[3]; /* unit vector pointing at source, in J2000 Cartesian coords */
    REAL8 cosRot; /* dot product of saved and requested source direction */
    REAL8 sinRot;
    REAL8 rotAxis[3]; /* cross product of saved and requested source direction */
    REAL8 tdiff; /* emission time difference between ref point and requested point */
    REAL8 euler1; // }
    REAL8 euler2; /* } z-x-z Euler angles */
    REAL8 euler3; // }
    REAL8 t1; // }
    REAL8 t2; /* } Time difference from rotation in z-x-z Euler angles */
    REAL8 t3; // }
    int j;
    double dt;
    
    /* check input */
    if ( !emit || !baryinput || !earth ) {
        XLALPrintError ("%s: invalid NULL input 'baryinput, 'emit' or 'earth'\n", __func__ );
        XLAL_ERROR ( XLAL_EINVAL );
    }

    /* check init */
    if ( fitCount < 1 ) {
        XLALPrintError ("%s: No coefficients found. Was InitApprox called?\n", __func__ );
        XLAL_ERROR ( XLAL_EINVAL );
    }
    
    sinTheta=sin(LAL_PI/2.0-baryinput->delta);
    s[2]=cos(LAL_PI/2.0-baryinput->delta);   /* s is vector that points towards source */
    s[1]=sinTheta*sin(baryinput->alpha);  /* in Cartesian coords based on J2000 */
    s[0]=sinTheta*cos(baryinput->alpha);  /* 0=x,1=y,2=z */
    
    cosRot = s[0]*saved_s[0] + s[1]*saved_s[1] + s[2]*saved_s[2];
    
    if (BaryEqual(baryinput,&saved_bary)==0 &&
        baryinput->tgps.gpsSeconds >= fitTimes[saved_idx] &&
        (saved_idx + 1 >= fitCount || 
           baryinput->tgps.gpsSeconds < fitTimes[saved_idx + 1] ) &&
        cosRot >= maxRot) {
    
        emit->roemer = saved_emit.roemer;
        emit->droemer = saved_emit.droemer;
        
        emit->erot = saved_emit.erot;
        emit->derot = saved_emit.derot;
        
        emit->shapiro = saved_emit.shapiro;
        emit->dshapiro = saved_emit.dshapiro;
        
        emit->tDot = saved_emit.tDot;
        
        dt = (double)(baryinput->tgps.gpsSeconds - fitTimes[saved_idx]) +
             1e-9*baryinput->tgps.gpsNanoSeconds;
        // Cross-product gives rotation direction
        rotAxis[0] = saved_s[1]*s[2] - saved_s[2]*s[1];
        rotAxis[1] = saved_s[2]*s[0] - saved_s[0]*s[2];
        rotAxis[2] = saved_s[0]*s[1] - saved_s[1]*s[0];
        sinRot = sqrt(rotAxis[0]*rotAxis[0] +
                      rotAxis[1]*rotAxis[1] +
                      rotAxis[2]*rotAxis[2]);
        euler1 = atan2(rotAxis[0]*rotAxis[1]*(1-cosRot) + rotAxis[2]*sinRot,
                       1 - (rotAxis[1]*rotAxis[1] + rotAxis[2]*rotAxis[2]) +
                       (rotAxis[1]*rotAxis[1] + rotAxis[2]*rotAxis[2])*cosRot);
        euler2 = asin(rotAxis[0]*rotAxis[2]*(1-cosRot) + rotAxis[1]*sinRot);
        euler3 = atan2(rotAxis[1]*rotAxis[2]*(1-cosRot) + rotAxis[2]*sinRot,
                       1 - (rotAxis[1]*rotAxis[1] + rotAxis[0]*rotAxis[0]) +
                       (rotAxis[1]*rotAxis[1] + rotAxis[0]*rotAxis[0])*cosRot);
        t1 = RotationFit(zCoef, saved_idx, saved_s, dt, euler1, &saved_bary, earth, &saved_emit);
        t2 = RotationFit(yCoef, saved_idx, saved_s, dt, euler2, &saved_bary, earth, &saved_emit);
        t3 = RotationFit(xCoef, saved_idx, saved_s, dt, euler3, &saved_bary, earth, &saved_emit);
        tdiff = t1 + t2 + t3;
        
        emit->deltaT = saved_emit.deltaT + tdiff;
        
        emit->te.gpsSeconds = saved_emit.te.gpsSeconds + (int) tdiff;
        emit->te.gpsNanoSeconds = saved_emit.te.gpsNanoSeconds + 1e9*(tdiff - (int) tdiff);

        for (j=0;j<3;j++){
            emit->rDetector[j] = saved_emit.rDetector[j];
            emit->vDetector[j] = saved_emit.vDetector[j];
        }
    } else {
        XLALBarycenter(emit, baryinput, earth);
        saved_emit = *emit;
        saved_bary = *baryinput;
        saved_s[0] = s[0];
        saved_s[1] = s[1];
        saved_s[2] = s[2];
        saved_idx = PickFit(baryinput->tgps.gpsSeconds);
        if (xCoef[saved_idx] == 0) {
            fprintf(stderr, "Loading coefs for %i.\n", baryinput->tgps.gpsSeconds);
            LoadCoefs(baryinput->tgps.gpsSeconds);
        }
    }
    
    return XLAL_SUCCESS;
    
} /* XLALBarycenterApprox() */
